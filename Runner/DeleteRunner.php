<?php


namespace Anchu\Restful\Runner;

use Illuminate\Http\Request;

class DeleteRunner extends Runner
{
    public function run(Request $request)
    {
        $this->beforeRun();
        $id = $request->route('id');
        $this->repository->delete($id, $this->filter->where);
        $this->afterRun();
        return $this->result;
    }
}
