<?php

namespace Anchu\Restful\Requests;

use Anchu\Restful\Models\Model;
use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{
    /**
     * @var string|Model
     * 子类使用举例：
     * public string|Model $model = TestModel::class;
     */
    public string|Model $model;

    /**
     * 在模型类中获取校验规则
     * @return array
     */
    public function rules()
    {
        return $this->model::rules();
    }

    /**
     * 在模型类中获取属性名称
     * @return array
     */
    public function attributes()
    {
        return $this->model::attributes();
    }


    public function covertKeys()
    {
        return $this->model::defaults();
    }


    /**
     * 补充输入默认值
     * @return void
     */
    protected function prepareForValidation()
    {
        $mergeArr = [];
        $keys = $this->covertKeys();
        foreach ($keys as $key => $default) {
            if ($this->has($key) && !$this->filled($key)) {
                $mergeArr[$key] = $default;
            }
        }
        if (!empty($mergeArr)) {
            $this->merge($mergeArr);
        }
    }
}
