<?php

namespace Anchu\Restful\Models\Demos;

use Anchu\Restful\Models\Columns\ColumnFactory;
use Anchu\Restful\Models\Keys\Key;
use Anchu\Restful\Models\Model;
use Anchu\Restful\Models\TableSchema;

class TestModel extends Model
{
    public static function table(): TableSchema
    {
        // TODO: Implement table() method.
        $column = new ColumnFactory();
        return new TableSchema(
            tableName: 'table_name',
            comment: '测试用',
            columns: [
            'job' => $column->string(label: '工作', type: 'string'),
            'duration' => $column->varchar(label: '用工时段', comment: '给临时工用', rule: 'string|max:100'),
            'salary' => $column->char(label: '薪水', comment: '用逗号隔开', rule: 'string'),
            'num' => $column->integer(label: '招聘人数', unsigned: false),
            'status' => $column->tinyInteger(label: '状态', comment: '1:保存；2:发布（上架）；3：已下架；4:违规下架.'),
            'experience' => $column->smallInteger(label: '工作经验', comment: '1：经验不限；2：1-3年；3：3-5年；4：5-10年；5：10年以上', default: 1),
            'payment' => $column->mediumInteger(label: '结薪方式', comment: '1：按月；2：按日', default: 1),
            'user_id' => $column->bigInteger(label: '用户id'),
            'yasi' => $column->numeric(label: '雅思成绩', rule: 'numeric|min:0|max:10'),
            'yasi_float' => $column->float(label: '雅思成绩1', rule: 'numeric|min:0|max:10'),
            'yasi_double' => $column->double(label: '雅思成绩2', rule: 'numeric|min:0|max:10'),
            'yasi_decimal' => $column->decimal(label: '雅思成绩3', rule: 'numeric|min:0|max:10'),
            'article' => $column->text(label: '雅思成绩3'),
            'article_tiny' => $column->text(label: '雅思成绩3', type: 'tinyText'),
            'article_medium' => $column->text(label: '雅思成绩3', type: 'mediumText'),
            'article_long' => $column->text(label: '雅思成绩3', type: 'longText'),
            'anonymous' => $column->enum(label: '雅思成绩3', in: ['0', '1'], comment: '0：真名；1：匿名'),
            'published_at' => $column->timestamp(label: '发布时间'),
            'gender' => $column->gender(),
            'state' => $column->status(),
            'person' => $column->person(),
            'mobile' => $column->mobile(),
        ],
            keys: [
                'index_normal' => new Key(
                    column: ['user_id']
                ),
                'index_unique' => new Key(
                    column: ['created_at'],
                    unique: true
                ),
                'index_union' => new Key(
                    column: ['created_at', 'updated_at'],
                    unique: true
                )
            ]
        );
    }
}
