<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 不可变长字段
 */
class CChar extends CString
{
    public function __construct(
        string $label,
        int $length = 255,
        bool $null = false,
        string $default = '',
        string $comment = '',
        string $rule = ''
    )
    {
        parent::__construct($label, $type = 'char', $length, $null, $default, $comment, $rule);
    }
}
