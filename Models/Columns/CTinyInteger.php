<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 * `num` tinyint DEFAULT NULL,
 */
class CTinyInteger extends CInteger
{
    public function __construct(
        string $label,
        string $comment = '',
        bool $null = false,
        int $default = 0,
        bool $unsigned = true,
        string $rule = ''
    )
    {
        parent::__construct(
            label: $label,
            comment: $comment,
            type: 'tinyInteger',
            null: $null,
            default: $default,
            unsigned: $unsigned,
            rule: $rule
        );
    }

    public function rule()
    {
        return $this->rule != '' ? $this->rule : ($this->unsigned ? 'integer|min:0|max:255' : 'integer|min:-128|max:127');
    }
}
