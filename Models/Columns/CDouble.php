<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CDouble extends CNumeric
{
    public function __construct(
        string $label, // 属性名称：必填
        string $comment = '',
        int $precision = 8,
        int $scale = 2,
        bool $null = false,
        float $default = 0,
        bool $unsigned = false,
        string $rule = '',
    )
    {
        parent::__construct(
            label: $label,
            comment: $comment,
            type: 'double',
            precision: $precision,
            scale: $scale,
            null: $null,
            default: $default,
            unsigned: $unsigned,
            rule: $rule
        );
    }
}
