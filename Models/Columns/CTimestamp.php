<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * timestamp
 * @package Anchu\Restful\Models\Columns
 */
class CTimestamp extends Column
{
    public bool $null;
    public string|null $default;
    public string $label;
    public string $comment;

    /**
     * CTimestamp constructor.
     * @param bool $null        : 是否允许空值
     * @param string|null $default   : 默认值设置
     * @param string $label     : 字段的名称，用于校验时的提示
     * @param string $comment   : 字段的附属说明，如：status:状态，1:保存；2:发布（上架）；3：已下架；4:违规下架
     */
    public function __construct(string $label, $null = true, $default = null, $comment = '')
    {
        $this->null = $null;
        $this->default = $default;
        $this->label = $label;
        // 这样做的目的是为了将label和comment分开：
        // $label : 状态
        // $comment : 1：提交， 2：通过，3：驳回
        // $this->comment = 状态 1：提交， 2：通过，3：驳回
        $this->comment = $comment == '' ? $label : trim($label . ' ' . $comment);
    }

    /**
     * @inheritDoc
     */
    public function rule()
    {
        // TODO: Implement rules() method.
        return 'string|date';
    }

    public function createColumn(string $tableName, string $columnName)
    {
        // TODO: Implement createColumn() method.
        $context = $this;
        Schema::table($tableName, function (Blueprint $table) use ($context, $columnName) {
            // 没有设置length的功能
            $table->timestamp($columnName)
                ->nullable($context->null)
                ->default($context->default)
                ->comment($context->comment);
        });
    }
}
