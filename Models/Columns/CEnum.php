<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 枚举类型
 * `status` enum('未付款','已付款','已发货','已送达','已收货') default null,
 */
class CEnum extends Column
{
    /**
     * CString constructor.
     * 必填：
     * @param string $label : 字段的名称，用于校验时的提示
     * @param array $in : 枚举列表：['未付款','已付款','已发货','已送达','已收货']
     * @param string $default : 默认值设置
     * 可默认：
     * @param bool $null : 是否允许空值
     * @param string $comment : 字段的附属说明，如：status:状态，1:保存；2:发布（上架）；3：已下架；4:违规下架
     */
    public function __construct(
        public string $label,
        public array $in,
        public bool $null = false,
        public string $default = '',
        public string $comment = ''
    )
    {
        $this->default = $default == '' ? $in[0] : $default;
        // 这样做的目的是为了将label和comment分开：
        // $label : 状态
        // $comment : 1：提交， 2：通过，3：驳回
        // $this->comment = 状态 1：提交， 2：通过，3：驳回
        $this->comment = $comment == '' ? $label : trim($label . ' ' . $comment);
    }

    /**
     * @inheritDoc
     */
    public function rule()
    {
        // TODO: Implement rules() method.
        return 'string|in:' . implode(',', $this->in);
    }

    public function createColumn(string $tableName, string $columnName)
    {
        // TODO: Implement createColumn() method.
        $context = $this;
        Schema::table($tableName, function (Blueprint $table) use ($context, $columnName) {
            // 没有设置length的功能
            $table->enum($columnName, $context->in)
                ->nullable($context->null)
                ->default($context->default)
                ->comment($context->comment);
        });
    }
}
