<?php

namespace Anchu\Restful\Tests;

use Tests\TestCase;

abstract class ApiTest extends TestCase
{

    private string $token;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        // 设置权限
        $this->header(['Authorization' => 'Authorization Bearer ' . $this->token]);
    }

    /**
     * 在实际使用过程中，需要继承此类，并实现此方法
     * @return string 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vdG9uZ2x1YmVlYXBpLmNuL2FwaS9sb2dpbiIsImlhdCI6MTY2MzkyNTYyNSwiZXhwIjoxNjYzOTI5MjI1LCJuYmYiOjE2NjM5MjU2MjUsImp0aSI6IlRRT1EwVkVjeWF5UU91ZEkiLCJzdWIiOiIxIiwicHJ2IjoiNDFkZjg4MzRmMWI5OGY3MGVmYTYwYWFlZGVmNDIzNDEzNzAwNjkwYyJ9.vm0CEsTF-ftgDxfoZMSCzxx1yZZopeBb3SL9OjgbsRQ'
     */
    abstract protected function setToken();

    // ajax 请求设置
    protected function header(array $headers = [])
    {
        $headers = array_merge($headers, [
            'X-Requested-With' => 'XMLHttpRequest',
            'Accept' => 'application/json',
        ]);
        $this->withHeaders($headers);
        return $this;
    }
}
